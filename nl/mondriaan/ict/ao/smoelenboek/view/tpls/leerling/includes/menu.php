<nav id="top">
    <ul>
        <li>
            <a href="?control=leerling&action=default">Home</a>
            <a href="?control=leerling&action=mijn_klas">Mijn klas</a>
            <a href="?control=leerling&action=gegevens">Gegevens</a>
            <a href="?control=leerling&action=wachtwoord">Wachtwoord</a>
            <a href="?control=leerling&action=uitloggen">Uitloggen</a>
        </li>
    </ul>
</nav>  
<nav id="left">
    <ul>
        <?php foreach ($klassen as $klas): ?>
            <li>
                <a href="?control=leerling&action=klas"><?= $klas->getNaam() ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>