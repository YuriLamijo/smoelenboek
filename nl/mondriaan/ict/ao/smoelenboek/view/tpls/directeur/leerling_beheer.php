<?php
include 'includes/header.php';
include 'includes/menu.php';
?>
<section id='content'>
    <table id="contacten">
        <caption>dit zijn alle leerlingen van de school voor ict</caption>
        <thead>
            <tr>
                <td>foto</td>
                <td>naam</td>
                <td>adres</td>
                <td>email</td>
                <td>telefoon nummer</td>
                <td>recht</td>
                <td colspan="3">acties</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contacten as $contact): ?>
                <tr>
                    <td>
                        <figure>
                            <img src="img/personen/<?= $contact->getFoto(); ?>" alt="de foto van <?= $contact->getNaam(); ?>" />
                        </figure>
                    </td>
                    <td><?= $contact->getNaam(); ?></td>
                    <td><?= $contact->getAdres(); ?></td>
                    <td><?= $contact->getEmail(); ?></td>
                    <td><?= $contact->getTelefoonnummer(); ?></td>
                    <td><?= $contact->getRecht(); ?></td>
                    <td title="reset het wachtwoord van dit contact naar qwerty"><a href='?control=directeur&action=resetww&id=<?= $contact->getId(); ?>'><img src="img/resetww.png"></a></td>
                    <td title="bewerk de contact gegevens van dit contact"><a href='?control=directeur&action=update&id=<?= $contact->getId(); ?>'><img src="img/bewerk.png"></a></td>
                    <td title="verwijder dit contact definitief"><a href='?control=directeur&action=delete&id=<?= $contact->getId(); ?>'><img src="img/verwijder.png"></a></td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td>
                    <a href='?control=directeur&action=add'>
                        <figure>
                            <img src="img/toevoegen.png" alt='voeg een leerling toe' title='voeg een leerling toe' />
                        </figure>
                    </a>
                </td>
                <td colspan='8'>voeg een leerling aan de school toe</td>
            </tr>
        </tbody>
    </table>
    <br id ="breaker" />
</section>
<?php
include 'includes/footer.php';
