<nav id="top">
    <ul>
        <li>
            <a href="?control=directeur&action=default">Home</a>
            <a href="?control=directeur&action=gegevens">Gegevens</a>
            <a href="?control=directeur&action=wachtwoord">Wachtwoord</a>
            <a href="?control=directeur&action=leerling_beheer">Leerling beheer</a>
            <a href="?control=directeur&action=docent_beheer">Docent beheer</a>
            <a href="?control=directeur&action=uitloggen">Uitloggen</a>
        </li>
    </ul>
</nav>  
<nav id="left">
    <ul>
        <?php foreach ($klassen as $klas): ?>
            <li>
                <a href="?directeur=docent&action=klas"><?= $klas->getNaam() ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>