<?php
include 'includes/header.php';
include 'includes/menu.php';
?>
<section id='content'>
    <form  method="post" id="gebruiker_form">
        <table >
            <caption>Detail gegevens van  <?= $contact->getNaam(); ?></caption>
            <tr>
                <td >gebruikersnaam</td>
                <td>
                    <input type="text" placeholder="vul verplicht een gebruikersnaam in." name="gn" value="<?= $contact->getGebruikersnaam(); ?>" >
                </td>
            </tr>
            <tr>
                <td >voornaam</td>
                <td>
                    <input type="text" placeholder="vul verplicht een gebruikersnaam in." name="vn" value="<?= $contact->getVoornaam(); ?>" >
                </td>
            </tr>
            <tr>
                <td >tussenvoegsel</td>
                <td>
                    <input type="text" placeholder="vul verplicht een gebruikersnaam in." name="tv" value="<?= $contact->getTussenvoegsel(); ?>" >
                </td>
            </tr>
            <tr>
                <td >achternaam</td>
                <td>
                    <input type="text" placeholder="vul verplicht een gebruikersnaam in." name="an" value="<?= $contact->getAchternaam(); ?>" >
                </td>
            </tr>
            <tr>
                <td >telefoon nummer</td>
                <td>
                    <input type="text" name="tel" placeholder="vul optioneel een intern nummer in." value="<?= $contact->getTelefoonnummer(); ?>" >
                </td>
            </tr>
            <tr>
                <td >adres</td>
                <td>
                    <input type="text" name="adres" placeholder="vul optioneel een extern nummer in."  value="<?= $contact->getAdres(); ?>">
                </td>
            </tr>
            <tr>
                <td >email</td>
                <td>
                    <input type="email" name="email"  placeholder="vul verplicht een emailadres in."  value="<?= $contact->getEmail(); ?>" >
                </td>
            </tr>
            <tr>
                <td>foto</td>
                <td>
                    <form method="post" id="foto_form" enctype="multipart/form-data">
                        <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
                        <figure id="details">
                            <img src="img/personen/<?= $contact->getFoto(); ?>" alt="mijn foto:  <?= $contact->getNaam(); ?>" />
                            <figcaption>
                                de huidige foto van <?= $contact->getNaam(); ?> 
                            </figcaption>  
                        </figure>
                        <div>
                            <input type="file" name="foto" accept="image/*"  />
                            <input type="submit" name="upload" value="load up" />
                        </div>
                    </form>
                </td>
            </tr>
        </table>
        <div>
            <input type="submit" value="verstuur" />
            <input type="reset" value ="reset" />
        </div>
    </form>  
    <br id ="breaker">
</section>
<?php
include 'includes/footer.php';
