<?php
include 'includes/header.php';
include 'includes/menu.php';
?>
<section id='content'>
    <table id="contacten">
        <thead>
        <caption>
            dit zijn alle leerligen van de school voor ict
        </caption>
        <tr>
            <td>foto</td>
            <td>naam</td>
            <td>email</td>
            <td>telefoon nummer</td>
            <td>opmerking</td>
            <td>klas</td>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($contacten as $contact): ?>
                <tr>
                    <td>
                        <figure>
                            <img src="img/personen/<?= $contact->getFoto(); ?>" alt="de foto van <?= $contact->getNaam(); ?>" title="Opmerking: <?= $contact->getOpmerking() ?>">
                        </figure>
                    </td>
                    <td>
                        <a href="?control=docent&action=details&id=<?= $contact->getId() ?>"><?= $contact->getNaam(); ?></a>
                    </td>
                    <td>
                        <a href="mailto: <?= $contact->getEmail(); ?>"><?= $contact->getEmail(); ?></a>
                    </td>
                    <td><?= $contact->getTelefoonnummer(); ?></td>
                    <td><?= $contact->getOpmerking(); ?></td>
                    <td><?= $contact->getKlassennaam(); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <br id ="breaker">
</section>
<?php
include 'includes/footer.php';
