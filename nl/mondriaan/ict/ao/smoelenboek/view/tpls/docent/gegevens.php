<?php
include 'includes/header.php';
include 'includes/menu.php';
?>
<section id='content'>
    <form  method="post" id="gebruiker_form">
        <table >
            <caption>Detail gegevens van  <?= $gebruiker->getNaam(); ?></caption>
            <tr>
                <td >gebruikersnaam</td>
                <td>
                    <input type="text" placeholder="vul verplicht een gebruikersnaam in." name="gn" value="<?= $gebruiker->getGebruikersnaam(); ?>" required>
                </td>
            </tr>
            <tr>
                <td >telefoon nummer</td>
                <td>
                    <input type="text" name="tel" placeholder="vul optioneel een intern nummer in." value="<?= $gebruiker->getTelefoonnummer(); ?>">
                </td>
            </tr>
            <tr>
                <td >email</td>
                <td>
                    <input type="email" name="email"  placeholder="vul verplicht een emailadres in."  value="<?= $gebruiker->getEmail(); ?>" required>
                </td>
            </tr>
        </table>
        <div>
            <input type="submit" value="verstuur" />
            <input type="reset" value ="reset" />
        </div>
    </form>  
    <br id ="breaker">
</section>
<?php
include 'includes/footer.php';
