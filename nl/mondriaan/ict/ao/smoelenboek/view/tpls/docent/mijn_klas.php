<?php
include 'includes/header.php';
include 'includes/menu.php';
?>
<section id='content'>
    <table id="contacten">
        <caption>dit zijn alle werknemers van de school voor ict</caption>
        <thead>
            <tr>
                <td>foto</td>
                <td>naam</td>
                <td>email</td>
                <td>telefoon nummer</td>
                <td>opmerking</td>
                <td>recht</td>
                <td colspan="1">actie</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contacten as $contact): ?>
                <tr>
                    <td>
                        <figure>
                            <div class="dropdown">
                                <span><img src="img/personen/<?= $contact->getFoto(); ?>" alt="de foto van <?= $contact->getNaam(); ?>"/></span>
                                <div class="dropdown-content">
                                    <p><?php
                                        if (is_null($contact->getOpmerking())) {
                                            echo 'geen Opmerking';
                                        } else {
                                            echo 'opmerking: ' . $contact->getOpmerking();
                                        }
                                        ?></p>
                                </div>
                            </div>
                        </figure>
                    </td>
                    <td><?= $contact->getNaam(); ?></td>
                    <td><?= $contact->getEmail(); ?></td>
                    <td><?= $contact->getTelefoonnummer(); ?></td>
                    <td><?= $contact->getOpmerking(); ?></td>
                    <td><?= $contact->getRecht(); ?></td>
                    <td title="voeg een opmerking toe aan dit contact"><a href='?control=docent&action=opmerking&id=<?= $contact->getId(); ?>'><img src="img/opmerking.png"></a></td>
                </tr>
<?php endforeach; ?>
        </tbody>
    </table>
    <br id ="breaker" />
</section>
<?php
include 'includes/footer.php';
