<nav id="top">
    <ul>
        <li>
            <a href="?control=docent&action=default">Home</a>
            <a href="?control=docent&action=mijn_klas">Mijn klas</a>
            <a href="?control=docent&action=gegevens">Gegevens</a>
            <a href="?control=docent&action=wachtwoord">Wachtwoord</a>
            <a href="?control=docent&action=uitloggen">Uitloggen</a>
        </li>
    </ul>
</nav>  
<nav id="left">
    <ul>
        <?php foreach ($klassen as $klas): ?>
            <li>
                <a href="?control=docent&action=<?php
                if ($gebruiker->getKlassen_id() === $klas->getId()) {
                    echo"klas&k_id=" . $gebruiker->getKlassen_id();
                } else {
                    echo"klas&k_id=" . $klas->getId();
                }
                ?>"><?= $klas->getNaam() ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>