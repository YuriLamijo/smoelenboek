<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Smoelenboek</title>
        <link rel="STYLESHEET" href="css/smoelenboek.css" type="text/css">
        <link rel="STYLESHEET" href="css/docent.css" type="text/css">
    </head>
    <body>
        <header>
            <figure>
                <img src="img/cld-logo.png" alt="ons schoolgebouw">
            </figure>
            <div>
                <p>Dit is de smoelennoek applicatie voor de middelbare school cld</p>
                <p>Momenteel is ingelogd: <em><?= $gebruiker->getNaam(); ?></em></p> 
                <p>Je hebt de rechten van: <em><?= $gebruiker->getRecht(); ?></em></p>
                <?= isset($boodschap) ? "<p id = 'boodschap'><em>$boodschap</em></p>" : "" ?>
            </div>
            <figure>
                <a href="?control=docent&action=foto" title='klik om je foto te wijzigen'> 
                    <img id="persoon" src="img/personen/<?= $gebruiker->getFoto() ?>">
                </a>
            </figure>
        </header>
        <section>

