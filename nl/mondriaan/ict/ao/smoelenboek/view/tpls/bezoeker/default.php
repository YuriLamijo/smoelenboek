<?php include 'includes/header.php';
include 'includes/menu.php';
?>
<section id='content'>
    <figure>
        <img src="img/school.jpg" alt="ons schoolgebouw" />
        <figcaption>
            <table>
                <th colspan="2">onze gegevens</th>
                <tr><td>school:</td><td>Christelijk lyceum delft</td></tr>
                <tr><td>straat:</td><td>Molenhuispark 1</td></tr>
                <tr><td>postcode:</td><td>2614 GE</td></tr>
                <tr><td>plaats:</td><td>Delf</td></tr>
                <tr><td>telefoon:</td><td>015 2684330</td></tr>
            </table>
        </figcaption>
    </figure>
    <br id ="breaker" />
</section>
<?php
include 'includes/footer.php';
