<?php

namespace nl\mondriaan\ict\ao\smoelenboek\models;

class DocentModel {

    private $control;
    private $action;
    private $db;

    public function __construct($control, $action) {
        $this->control = $control;
        $this->action = $action;
        $this->db = new \PDO(DATA_SOURCE_NAME, DB_USERNAME, DB_PASSWORD);
        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->startSessie();
    }

    private function startSessie() {
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    public function isGerechtigd() {
        //controleer of er ingelogd is. Ja, kijk of de gebuiker deze controller mag gebruiken 
        if (isset($_SESSION['gebruiker']) && !empty($_SESSION['gebruiker'])) {
            $gebruiker = $_SESSION['gebruiker'];
            if ($gebruiker->getRecht() == "docent") {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public function getGebruiker() {
        return $_SESSION['gebruiker'];
    }

    public function uitloggen() {
        $_SESSION = array();
        session_destroy();
    }

    function isPostLeeg() {
        return empty($_POST);
    }

    public function wijzigGegevens() {
        $gebruikersnaam = filter_input(INPUT_POST, 'gn');
        $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
        $adres = filter_input(INPUT_POST, 'adres');
        $telefoon_nummer = filter_input(INPUT_POST, 'tel');

        if (empty($email) || empty($gebruikersnaam)) {
            return REQUEST_FAILURE_DATA_INCOMPLETE;
        }

        if ($email === false) {
            return REQUEST_FAILURE_DATA_INVALID;
        }

        $gebruiker_id = $this->getGebruiker()->getId();

        $sql = "UPDATE `contacten` SET gebruikersnaam=:gebruikersnaam,adres=:adres,telefoon_nummer=:telefoon_nummer,email=:email where `contacten`.`id`= :gebruiker_id ";
        $stmnt = $this->db->prepare($sql);
        $stmnt->bindParam(':gebruikersnaam', $gebruikersnaam);
        $stmnt->bindParam(':telefoon_nummer', $telefoon_nummer);
        $stmnt->bindParam(':adres', $adres);
        $stmnt->bindParam(':email', $email);
        $stmnt->bindParam(':gebruiker_id', $gebruiker_id);

        try {
            $stmnt->execute();
        } catch (\PDOEXception $e) {
            return REQUEST_FAILURE_DATA_INVALID;
        }

        $aantalGewijzigd = $stmnt->rowCount();
        if ($aantalGewijzigd === 1) {
            $this->updateGebruiker();
            return REQUEST_SUCCESS;
        }
        return REQUEST_NOTHING_CHANGED;
    }
    
    public function addOpmerking() {
        $opmerking = filter_input(INPUT_POST, 'op');

        $contact_id = $this->getContact()->getId();

        $sql = "UPDATE `contacten` SET opmerking=:opmerking where `contacten`.`id`= :contact_id ";
        $stmnt = $this->db->prepare($sql);
        $stmnt->bindParam(':opmerking', $opmerking);
        $stmnt->bindParam(':contact_id', $contact_id);

        try {
            $stmnt->execute();
        } catch (\PDOEXception $e) {
            return REQUEST_FAILURE_DATA_INVALID;
        }

        $aantalGewijzigd = $stmnt->rowCount();
        if ($aantalGewijzigd === 1) {
            $this->updateGebruiker();
            return REQUEST_SUCCESS;
        }
        return REQUEST_NOTHING_CHANGED;
    }

    private function updateGebruiker() {
        $gebruiker_id = $this->getGebruiker()->getId();
        $sql = "SELECT * FROM `contacten` WHERE `contacten`.`id`=:gebruiker_id";
        $stmnt = $this->db->prepare($sql);
        $stmnt->bindParam(':gebruiker_id', $gebruiker_id);
        $stmnt->setFetchMode(\PDO::FETCH_CLASS, __NAMESPACE__ . '\db\Contact');
        $stmnt->execute();
        $_SESSION['gebruiker'] = $stmnt->fetch(\PDO::FETCH_CLASS);
    }

    public function wijzigWw() {
        $ww = filter_input(INPUT_POST, 'ww');
        $nww1 = filter_input(INPUT_POST, 'nww1');
        $nww2 = filter_input(INPUT_POST, 'nww2');

        if ($ww === null || $nww1 === null || $nww2 === null) {
            return REQUEST_FAILURE_DATA_INCOMPLETE;
        }

        if (empty($nww1) || empty($nww2) || empty($ww)) {
            return REQUEST_FAILURE_DATA_INCOMPLETE;
        }

        if ($_POST['nww1'] !== $_POST['nww2']) {
            return REQUEST_FAILURE_DATA_INVALID;
        }

        $hww = $this->getGebruiker()->getWachtwoord();

        if ($hww !== $ww) {
            return REQUEST_FAILURE_DATA_INVALID;
        }

        if ($nww1 === $ww) {
            return REQUEST_NOTHING_CHANGED;
        }

        $id = $this->getGebruiker()->getId();
        $sql = "UPDATE `contacten` SET `contacten`.`wachtwoord` = :nww WHERE `contacten`.`id`= :id";
        $stmnt = $this->db->prepare($sql);
        $stmnt->bindParam(':id', $id);
        $stmnt->bindParam(':nww', $nww1);
        $stmnt->execute();
        $aantalGewijzigd = $stmnt->rowCount();

        if ($aantalGewijzigd === 1) {
            $this->updateGebruiker();
            return REQUEST_SUCCESS;
        }
        return REQUEST_NOTHING_CHANGED;
    }

    public function getContacten() {
        $sql = 'SELECT `contacten`.*, 
               `klassen`.`naam` AS klas_naam 
                FROM `contacten` , `klassen` 
                WHERE `contacten`.`recht`=\'leerling\' AND `contacten`.`klassen_id` = `klassen`.`id` 
                ORDER BY klas_naam DESC, achternaam ASC';
        $stmnt = $this->db->prepare($sql);
        $stmnt->execute();
        $contacten = $stmnt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\db\Contact');
        return $contacten;
    }

    public function getMijnklas() {
        $sql = 'SELECT * FROM `contacten` WHERE klassen_id = 1 ORDER BY `recht` DESC';
        $stmnt = $this->db->prepare($sql);
        $stmnt->execute();
        $contacten = $stmnt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\db\Contact');
        return $contacten;
    }

    public function getContact() {
      $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

      if ($id === null) {
      return REQUEST_FAILURE_DATA_INCOMPLETE;
      }
      if ($id === false) {
      return REQUEST_FAILURE_DATA_INVALID;
      }

      $sql = "SELECT * FROM `contacten` WHERE `contacten`.`id`= :id";
      $stmnt = $this->db->prepare($sql);
      $stmnt->bindParam(':id', $id);
      $stmnt->execute();
      $contact = $stmnt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\db\Contact');
      if (empty($contact)) {
      return REQUEST_FAILURE_DATA_INVALID;
      }
      return $contact[0];
      } 

    public function getKlassen() {
        $sql = 'SELECT * FROM `klassen` ORDER BY naam';
        $stmnt = $this->db->prepare($sql);
        $stmnt->execute();
        $klassen = $stmnt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\db\Klas');
        return $klassen;
    }
    
    public function getLeerlingen() {
        $k_id = filter_input(INPUT_GET, 'k_id',FILTER_VALIDATE_INT);
        
        $sql = 'SELECT * FROM `contacten` WHERE klassen_id =:k_id ORDER BY `recht` DESC';
        $stmnt = $this->db->prepare($sql);
        $stmnt->bindParam(':k_id',$k_id);
        $stmnt->execute();
        $contacten = $stmnt->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__ . '\db\Contact');
        return $contacten;
    }

}
