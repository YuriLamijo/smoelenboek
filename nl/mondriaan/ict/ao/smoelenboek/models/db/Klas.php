<?php
namespace nl\mondriaan\ict\ao\smoelenboek\models\db;

class Klas {
    private $id;
    private $omschrijving;
    private $foto;
    private $naam;
    private $mentor_id;
    
    public function __construct()
    {
        $this->id = filter_var($this->id,FILTER_VALIDATE_INT);
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getOmschrijving() 
    {
        return $this->omschrijving;
    }
    
    public function getFoto()
    {
        return $this->foto;
    }
    
    public function getNaam()
    {
        return $this->naam;
    }
    
    public function getMentor_id()
    {
        return $this->mentor_id;
    }
}