<?php

namespace nl\mondriaan\ict\ao\smoelenboek\models\db;

class Contact {

    private $id;
    private $gebruikersnaam;
    private $wachtwoord;
    private $voornaam;
    private $tussenvoegsel;
    private $achternaam;
    private $email;
    private $telefoon_nummer;
    private $adres;
    private $recht;
    private $foto;
    private $klassen_id;
    private $klas_naam;
    private $opmerking;

    public function __construct() {
        $this->id = filter_var($this->id, FILTER_VALIDATE_INT);
        $this->klassen_id = filter_var($this->klassen_id, FILTER_VALIDATE_INT);
    }

    public function getId() {
        return $this->id;
    }

    public function getKlassen_id() {
        return $this->klassen_id;
    }
    
    public function getKlassennaam() {
        return $this->klas_naam;
    }

    public function getVoornaam() {
        return $this->voornaam;
    }

    public function getTussenvoegsel() {
        return $this->tussenvoegsel;
    }

    public function getAchternaam() {
        return $this->achternaam;
    }

    public function getNaam() {
        return "$this->voornaam $this->tussenvoegsel $this->achternaam";
    }

    public function getTelefoonnummer() {
        return $this->telefoon_nummer;
    }
    
    public function getEmail() {
        return $this->email;
    }

    public function getAdres() {
        return $this->adres;
    }

    public function getOpmerking() {
        return $this->opmerking;
    }

    public function getRecht() {
        return $this->recht;
    }

    public function getGebruikersnaam() {
        return $this->gebruikersnaam;
    }

    public function getWachtwoord() {
        return $this->wachtwoord;
    }

    public function getFoto() {
        return $this->foto;
    }

}
