<?php

namespace nl\mondriaan\ict\ao\smoelenboek\controls;

use nl\mondriaan\ict\ao\smoelenboek\models as MODELS;
use nl\mondriaan\ict\ao\smoelenboek\view as VIEW;
use nl\mondriaan\ict\ao\smoelenboek\utils\Foto as FOTO;

class DirecteurController {

    private $action;
    private $control;
    private $view;
    private $model;

    public function __construct($control, $action) {
        $this->control = $control;
        $this->action = $action;

        $this->view = new VIEW\View();
        $this->model = new MODELS\DirecteurModel($control, $action);

        $isGerechtigd = $this->model->isGerechtigd();

        if ($isGerechtigd != true) {
            $this->model->loguit();
            $this->forward('default', "bezoeker");
        }
    }

    /**
     * execute vertaalt de action variable dynamisch naar een handler van de specifieke controller.
     * als de handler niet bestaat wordt de default als action ingesteld en
     * wordt de taak overgedragen aan de defaultAction handler. defaultAction bestaat altijd wel
     */
    public function execute() {
        $opdracht = $this->action . 'Action';
        if (!method_exists($this, $opdracht)) {
            $opdracht = 'defaultAction';
            $this->action = 'default';
        }
        $this->$opdracht();
        $this->view->setAction($this->action);
        $this->view->setControl($this->control);
        $this->view->toon();
    }

    private function forward($action, $control = null) {
        if ($control === null) {
            $this->action = $action;
            $controller = $this;
        } else {
            $klasseNaam = __NAMESPACE__ . '\\' . ucFirst($control) . 'Controller';
            $controller = new $klasseNaam($control, $action);
        }
        $controller->execute();
        exit();
    }

    private function uitloggenAction() {
        $this->model->uitloggen();
        $this->forward('default', 'bezoeker');
    }

    private function defaultAction() {
        $gebruiker = $this->model->getGebruiker();
        $this->view->set('gebruiker', $gebruiker);
        $contacten = $this->model->getContacten();
        $this->view->set('contacten', $contacten);
        $klassen = $this->model->getKlassen();
        $this->view->set("klassen", $klassen);
    }
    
    private function addAction() {
        if ($this->model->isPostLeeg()) {
            $this->view->set("boodschap", "Vul gegevens in van een nieuwe medewerker");
        } else {
            $result = $this->model->addContact();
            switch ($result) {
                case IMAGE_FAILURE_SIZE_EXCEEDED:
                    $this->view->set("boodschap", "Contact is niet toegevoegd. Foto te groot. Kies kleinere foto.");
                    $this->view->set('form_data', $_POST);
                    break;
                case IMAGE_FAILURE_TYPE:
                    $this->view->set("boodschap", "Contact is niet toegevoegd. foto niet van jpg, gif of png formaat.");
                    $this->view->set('form_data', $_POST);
                    break;
                case REQUEST_FAILURE_DATA_INCOMPLETE:
                    $this->view->set("boodschap", "Contact is niet toegevoegd. Niet alle vereiste data ingevuld.");
                    $this->view->set('form_data', $_POST);
                    break;
                case REQUEST_FAILURE_DATA_INVALID:
                    $this->view->set("boodschap", "Contact is niet toegevoegd. Er is foutieve data ingestuurd (bv gebruikersnaam bestaat al).");
                    $this->view->set('form_data', $_POST);
                    break;
                case REQUEST_SUCCESS:
                    $this->view->set("boodschap", "Contact is toegevoegd.");
                    $this->forward("beheer");
                    break;
            }
        }
        $klassen = $this->model->getKlassen();
        $this->view->set("klassen", $klassen);
        $contacten = $this->model->getContacten();
        $this->view->set('contacten', $contacten);
        $gebruiker = $this->model->getGebruiker();
        $this->view->set('gebruiker', $gebruiker);
    }

    private function gegevensAction() {
        if ($this->model->isPostLeeg()) {
            $this->view->set("boodschap", "Wijzig hier je  gegevens");
        } else {
            $result = $this->model->wijzigGegevens();
            switch ($result) {
                case REQUEST_SUCCESS:
                    $this->view->set('boodschap', 'wijziging gelukt');
                    break;
                case REQUEST_FAILURE_DATA_INCOMPLETE:
                    $this->view->set("boodschap", "De gegevens waren incompleet. Vul compleet in!");
                    break;
                case REQUEST_NOTHING_CHANGED:
                    $this->view->set("boodschap", "Er was niets te wijzigen ");
                    break;
                case REQUEST_FAILURE_DATA_INVALID:
                    $this->view->set("boodschap", "gebruikersnaam is al in gebruik, kies een andere waarde.");
                    break;
            }
        }
        $gebruiker = $this->model->getGebruiker();
        $this->view->set('gebruiker', $gebruiker);
        $klassen = $this->model->getKlassen();
        $this->view->set("klassen", $klassen);
    }
    
    

    private function fotoAction() {

        if ($this->model->isPostLeeg()) {
            $this->view->set("boodschap", "Wijzig hier je foto");
        } else {
            $afbeeldingInfo = FOTO::isAfbeeldingGestuurd();
            switch ($afbeeldingInfo) {
                case IMAGE_NOTHING_UPLOADED:
                    $this->view->set("boodschap", "er is helemaal geen upload gedaan!!");
                    break;
                case IMAGE_FAILURE_SIZE_EXCEEDED:
                    $this->view->set("boodschap", "het door jouw ge-uploade bestand is te groot!!");
                    break;
                case IMAGE_FAILURE_TYPE:
                    $this->view->set("boodschap", "het door jou geuploade bestand is geen afbeelding (jpg, png, gif)!!");
                    break;
                case IMAGE_SUCCES:
                    $result = $this->model->wijzigFoto();
                    switch ($result) {
                        case REQUEST_NOTHING_CHANGED:
                        case IMAGE_FAILURE_SAVE_FAILED:
                            $this->view->set('boodschap', 'er is een serverfout, de afbeelding kan niet opgeslagen worden.');
                            break;
                        case REQUEST_SUCCESS:
                            $this->view->set('boodschap', 'de foto is succesvol gewijzigd');
                            $this->forward('default');
                    }
                    break;
            }
        }
        $gebruiker = $this->model->getGebruiker();
        $this->view->set('gebruiker', $gebruiker);
    }
    
    private function resetwwAction() {
        $result = $this->model->resetWw();
        switch ($result) {
            case REQUEST_FAILURE_DATA_INCOMPLETE:
                $this->view->set('boodschap', 'geen te gewijziged contact gegeven, dus niets gewijziged');
                break;
            case REQUEST_FAILURE_DATA_INVALID:
                $this->view->set('boodschap', 'te gewijziged contact bestaat niet');
                break;
            case REQUEST_NOTHING_CHANGED:
                $this->view->set('boodschap', 'Er is niets gewijziged wachtwoord is al qwerty.');
                break;
            case REQUEST_SUCCESS:
                $this->view->set('boodschap', 'Wachtwoord is gewijzigd.');
                break;
        }
            $this->forward('leerling_beheer');
    }

    private function wachtwoordAction() {
        $gebruiker = $this->model->getGebruiker();
        $this->view->set('gebruiker', $gebruiker);
        if ($this->model->isPostLeeg()) {
            $this->view->set("boodschap", "Wijzig hier je wachtwoord");
        } else {
            $result = $this->model->wijzigWw();
            switch ($result) {
                case REQUEST_SUCCESS:
                    $this->view->set('boodschap', 'wijziging wachtwoord gelukt');
                    break;
                case REQUEST_FAILURE_DATA_INVALID:
                    $this->view->set("boodschap", "nieuwe wachtwoord niet identiek of oude wachtwoord fout. Poog opnieuw!");
                    break;
                case REQUEST_FAILURE_DATA_INCOMPLETE:
                    $this->view->set("boodschap", "Niet alle velden ingevuld!");
                    break;
                case REQUEST_NOTHING_CHANGED:
                    $this->view->set("boodschap", "Er was niets te wijzigen");
                    break;
            }
        }
        $klassen = $this->model->getKlassen();
        $this->view->set("klassen", $klassen);
    }
    
    private function updateAction() {
        if ($this->model->isPostLeeg()) {
            $this->view->set("boodschap", "Wijzig hier de contact gegevens");
        } else {
            $result = $this->model->updateGegevens();
            switch ($result) {
                case REQUEST_FAILURE_DATA_INCOMPLETE;
                    $this->view->set('boodschap', 'geen te gewijziged contact gegevens gegeven, dus niets gewijziged');
                    break;
                case REQUEST_FAILURE_DATA_INVALID;
                    $this->view->set('boodschap', 'te gewijziged gegevens bestaan niet');
                    break;
                case REQUEST_NOTHING_CHANGED;
                    $this->view->set('boodschap', 'Er is niets gewijziged.');
                    break;
                case REQUEST_SUCCESS;
                    $this->view->set('boodschap', 'Contact gegevens zijn gewijzigd.');
                    break;
            }
        }
        $gebruiker = $this->model->getGebruiker();
        $this->view->set('gebruiker', $gebruiker);
        $contact = $this->model->getContact();
        $this->view->set('contact', $contact);
        $klassen = $this->model->getKlassen();
        $this->view->set("klassen", $klassen);
    }
    
    private function deleteAction() {
        $result = $this->model->deleteContact();
        switch ($result) {
            case REQUEST_FAILURE_DATA_INCOMPLETE:
                $this->view->set('boodschap', 'geen te verwijderen contact gegeven, dus niets verwijderd');
                break;
            case REQUEST_FAILURE_DATA_INVALID:
                $this->view->set('boodschap', 'te verwijderen contact bestaat niet');
                break;
            case REQUEST_NOTHING_CHANGED:
                $this->view->set('boodschap', 'Er is niets verwijderd reden onbekend.');
                break;
            case REQUEST_SUCCESS:
                $this->view->set('boodschap', 'Contact verwijderd.');
                break;
        }
        $this->forward('beheer');
    }

    private function mijn_klasAction() {
        $gebruiker = $this->model->getGebruiker();
        $this->view->set('gebruiker', $gebruiker);
        $contacten = $this->model->getMijnklas();
        $this->view->set('contacten', $contacten);
        $klassen = $this->model->getKlassen();
        $this->view->set("klassen", $klassen);
    }
    
    private function klasAction(){
        $gebruiker = $this->model->getGebruiker();
        $this->view->set('gebruiker', $gebruiker);
        $klassen = $this->model->getKlassen();
        $this->view->set("klassen", $klassen);
    }
    
    private function leerling_beheerAction(){
        $gebruiker = $this->model->getGebruiker();
        $this->view->set('gebruiker', $gebruiker);
        $contacten = $this->model->getContacten();
        $this->view->set('contacten', $contacten);
        $klassen = $this->model->getKlassen();
        $this->view->set("klassen", $klassen);
    }
    
    private function docent_beheerAction(){
        $gebruiker = $this->model->getGebruiker();
        $this->view->set('gebruiker', $gebruiker);
        $contacten = $this->model->docentBeheer();
        $this->view->set('contacten', $contacten);
        $klassen = $this->model->getKlassen();
        $this->view->set("klassen", $klassen);
    }
    
}
